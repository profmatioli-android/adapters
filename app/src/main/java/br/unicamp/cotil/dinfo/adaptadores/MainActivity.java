package br.unicamp.cotil.dinfo.adaptadores;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
    private ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.produtos,
                android.R.layout.simple_list_item_1
                );

        setListAdapter(adapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        CharSequence produto = adapter.getItem(position);
        Toast.makeText(this,produto, Toast.LENGTH_SHORT).show();
    }
}
